﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeDisplay : MonoBehaviour {

	[Header("Proportys")]
	public float speed = .2f;

	private float currentSize;

	// Use this for initialization
	void Start () {
		currentSize = transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update () {
		currentSize += (speed * Time.deltaTime);

		transform.localScale = new Vector3(currentSize,currentSize,currentSize);
		if (currentSize >= 1f)
		{
			Destroy(this.gameObject);
		}
	}
}
