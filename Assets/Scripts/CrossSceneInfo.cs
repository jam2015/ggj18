﻿/* Created by: Matthew George
 */

using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// 
/// </summary>
public class CrossSceneInfo : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebugCrossSceneInfo = false;
    private string debugScriptNameCrossSceneInfo = "CrossSceneInfo";
    #endregion

    #region Public
    public static CrossSceneInfo SINGLETON = null;

    [SerializeField] private GameObject looseScreen = null;
    [SerializeField] private string nextSceneName;
    #endregion

    #region Private
    private string sceneName;
    private bool won = false;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// The the seperate end screen scene.
    /// </summary>
    /// <param name="win">Should the loaded end screen be a game over screen or victory screen?</param>
    public void LoadEndScreen(bool win)
    {
        SceneManager.LoadSceneAsync("endScreen");
        won = win;
    }
    /// <summary>
    /// Shows the game over screen that is in this same level.
    /// </summary>
    public void ShowLooseScreen()
    {
        if (looseScreen != null) looseScreen.SetActive(true);
        else LoadEndScreen(false);
    }

    /// <summary>
    /// Loads the next level.
    /// </summary>
	public void LoadNextScene()
	{
		SceneManager.LoadSceneAsync(nextSceneName);
        if (nextSceneName == "endScreen")
        {
            won = true;
            nextSceneName = "startScreen";
        }
        else Destroy(gameObject);
    }
    /// <summary>
    /// Reload this same level.
    /// </summary>
	public void ReloadCurrent()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene(SceneName);
        Destroy(gameObject);
    }
    /// <summary>
    /// Loads up the start screen.
    /// </summary>
    public void LoadStartScreen()
    {
        SceneManager.LoadScene("startScreen");
        Destroy(gameObject);
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Use Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        if (isDebugCrossSceneInfo) Debug.Log(((this.GetType().Name != debugScriptNameCrossSceneInfo) ? this.GetType().Name + "->" : "") + debugScriptNameCrossSceneInfo + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintWarningDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogWarning(((this.GetType().Name != debugScriptNameCrossSceneInfo) ? this.GetType().Name + "->" : "") + debugScriptNameCrossSceneInfo + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintErrorDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogError(((this.GetType().Name != debugScriptNameCrossSceneInfo) ? this.GetType().Name + "->" : "") + debugScriptNameCrossSceneInfo + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    public string SceneName
    {
        get
        {
            return sceneName;
        }
    }
    public string NextSceneName
    {
        get
        {
            return nextSceneName;
        }
    }
    public bool Won
    {
        get
        {
            return won;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    void Awake()
    {
        PrintDebugMsg("Debugging enabled.");

        if (CrossSceneInfo.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg("More than one CrossSceneInfo singleton found!");

        sceneName = SceneManager.GetActiveScene().name;

        DontDestroyOnLoad(gameObject);

        if (looseScreen != null) looseScreen.SetActive(false);
    }
    #endregion
}