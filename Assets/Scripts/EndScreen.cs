﻿/* Created by: Matthew George
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// 
/// </summary>
public class EndScreen : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebugEndScreen = false;
    private string debugScriptNameEndScreen = "EndScreen";
    #endregion

    #region Public
    [SerializeField] private Button replayButton = null;
    [SerializeField] private Button nextLevelButton = null;
    [SerializeField] private Sprite startMenuButtonSprite = null;
    [SerializeField] private Sprite nextButtonSprite = null;
    [SerializeField] private Button startScreenButton = null;
    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    public void ReloadLevel()
    {
        SceneManager.LoadScene(CrossSceneInfo.SINGLETON.SceneName);
        Destroy(CrossSceneInfo.SINGLETON.gameObject);
    }
    public void LoadNextLevel()
    {
        SceneManager.LoadScene(CrossSceneInfo.SINGLETON.NextSceneName);
        Destroy(CrossSceneInfo.SINGLETON.gameObject);
    }
    public void LoadStartScreen()
    {
        SceneManager.LoadScene("startScreen");
        if (CrossSceneInfo.SINGLETON != null) Destroy(CrossSceneInfo.SINGLETON.gameObject);
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Use Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        if (isDebugEndScreen) Debug.Log(((this.GetType().Name != debugScriptNameEndScreen) ? this.GetType().Name + "->" : "") + debugScriptNameEndScreen + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintWarningDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogWarning(((this.GetType().Name != debugScriptNameEndScreen) ? this.GetType().Name + "->" : "") + debugScriptNameEndScreen + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintErrorDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogError(((this.GetType().Name != debugScriptNameEndScreen) ? this.GetType().Name + "->" : "") + debugScriptNameEndScreen + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties

    #endregion
    #endregion

    #region Start & Update Functions
    void Awake()
    {
        PrintDebugMsg("Debugging enabled.");
    }
    private void Start()
    {
        if(!CrossSceneInfo.SINGLETON.Won)
        {
            replayButton.gameObject.SetActive(true);
            nextLevelButton.gameObject.SetActive(false);
        }
        else
        {
            if (CrossSceneInfo.SINGLETON.NextSceneName == "startScreen")
            {
                replayButton.gameObject.SetActive(false);
                nextLevelButton.gameObject.SetActive(true);
                nextLevelButton.GetComponentInChildren<Text>().text = "Start Screen";
                nextLevelButton.GetComponent<Image>().sprite = startMenuButtonSprite;
                startScreenButton.gameObject.SetActive(false);

            }
            else
            {
                replayButton.gameObject.SetActive(false);
                nextLevelButton.gameObject.SetActive(true);
                nextLevelButton.GetComponentInChildren<Text>().text = "Next Level";
                nextLevelButton.GetComponent<Image>().sprite = nextButtonSprite;
                startScreenButton.gameObject.SetActive(true);
            }
        }
    }
    #endregion
}