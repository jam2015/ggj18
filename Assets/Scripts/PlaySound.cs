﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour {
	private AudioSource audS;

	// Use this for initialization
	void Start () {
		audS = GetComponent<AudioSource>();
		if (!audS.isPlaying)
		{
			audS.Play();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!audS.isPlaying)
		{
			Destroy(this.gameObject);
		}
	}
}
