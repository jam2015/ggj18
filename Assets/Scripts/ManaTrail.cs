﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaTrail : MonoBehaviour {
	[Header("Proportys")]
	public float range=2f;
	public float stopDistance=.1f;
	public float speed = 1f;
	public LayerMask relayLayerMask;
	public float offset=-.2f;

	[Header("Relays")]
	public List<GameObject> movePath;
	private GameObject startRelay;

	[Header("Tags")]
	public string relayTag="Relay";
	public string startTag = "Mana";

	private int curNode = 0;
	private Transform moveTarget;


	// Use this for initialization
	void Start () {
		startRelay = GameObject.FindGameObjectWithTag(startTag);
		BuildPath();
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance(movePath[curNode].transform.position,transform.position)<=stopDistance)
		{
			if (curNode+1 < movePath.Count)
			{
				curNode++;
			}
			else
			{
				Die();
			}
		}
		Move();
	}

	public void BuildPath()
	{
		movePath = new List<GameObject>();
		curNode = 0;
		GameObject currObject = startRelay;
		movePath.Add(currObject);

		bool endOfLine = false;
		
		while (endOfLine == false)
		{
			GameObject nextObject = FindClosestRelay(currObject);
			if (nextObject == currObject)
			{
				endOfLine = true;
			}
			else
			{
				movePath.Add(nextObject);
				currObject = nextObject;
			}
		}
	}

	public GameObject FindClosestRelay(GameObject cur)
	{
		GameObject closestObject = cur;

		Collider[] nearObjects;
		float lastDist = Mathf.Infinity;
		//nearObjects = Physics2D.OverlapCircleAll(point,range,relayLayerMask);
		nearObjects = Physics.OverlapSphere(cur.transform.position, range);

		foreach(Collider col in nearObjects)
		{
			if (movePath.Contains(col.gameObject)==false&&col.gameObject.tag == relayTag)
			{
				float curDist = Vector3.Distance(col.transform.position, cur.transform.position);
				if (curDist < lastDist)
				{
					closestObject = col.gameObject;
					lastDist = curDist;
				}
			}
		}

		return closestObject;
	}

	public void Die()
	{
		GameController.SINGLETON.manaActive = false;
		Destroy(this.gameObject);
	}

	private void Move()
	{
		GameObject target = movePath[curNode].gameObject;
		Vector3 movement = target.transform.position-transform.position;
		movement.Normalize();
		movement = new Vector3(movement.x, movement.y, movement.z + offset);

		movement = movement * speed * Time.deltaTime;
		transform.Translate(movement);
	}

}
