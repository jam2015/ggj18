﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class GameController : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebugGameController = false;
    private string debugScriptNameGameController = "GameController";
    #endregion

    #region Public
    [Header("Properties")]
    public static GameController SINGLETON = null;
    public GameObject manaStream;
    public bool manaActive = false;
    public float offset = -0.2f;
    public bool paused = false;
    public float particalCalmTime = .5f;
    public GameObject riftPartical = null;

    [Header("Soundeffects")]
    public GameObject manaSound;
    public GameObject relaySound;
    public GameObject relayReclaimSound;

    [Header("Refrence")]
    [SerializeField] private LayerMask relayLayer;
    [SerializeField] private LayerMask relayObjLayer;
    [SerializeField] private GameObject relay = null;
    public Canvas pauseScreen;
    #endregion

    #region Private
    private GameObject spawnedRiftPartical = null;
    private Animator riftAnimator = null;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Tells the mana stream to start flowing.
    /// </summary>
    private void StartMana()
    {
        if (!manaActive)
        {
            PrintDebugMsg("Starting mana stream...");
            GameObject stream = Instantiate(manaStream, GameObject.FindGameObjectWithTag("Mana").transform.position,
                new Quaternion());
            Vector3 sPos = new Vector3(stream.transform.position.x, stream.transform.position.y, stream.transform.position.z + offset);
            stream.transform.position = sPos;
            manaActive = true;
            if (manaSound != null) Instantiate(manaSound, transform.position, new Quaternion());
            riftAnimator.Play("Rift", 0, 0);
            if(spawnedRiftPartical == null) spawnedRiftPartical = Instantiate(riftPartical, riftAnimator.transform.position, Quaternion.identity);
            else
            {
                for (int i = 0; i < spawnedRiftPartical.transform.childCount; i++)
                {
                    ParticleSystem particle = spawnedRiftPartical.transform.GetChild(i).GetComponent<ParticleSystem>();
                    if (particle != null) particle.emissionRate = 60;
                }
            }

            Invoke("CalmRiftPartical", particalCalmTime);
        }
    }
    private void CalmRiftPartical()
    {
        for(int i = 0; i < spawnedRiftPartical.transform.childCount; i++)
        {
            ParticleSystem particle = spawnedRiftPartical.transform.GetChild(i).GetComponent<ParticleSystem>();
            if(particle != null) particle.emissionRate = 0;
        }
    }

    /// <summary>
    /// Spawns a relay where the user's mouse is if there are relays available.
    /// </summary>
    private void SpawnRelay()
    {
        if (relay == null)
        {
            PrintErrorDebugMsg("No relay object assigned!");
            return;
        }

        PrintDebugMsg("Trying to spawn relay...");
		if (!manaActive&&!paused)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 100, relayObjLayer))
			{
				PrintDebugMsg("Hit a relay.");
				//GameObject manaTrail = GameObject.FindGameObjectWithTag("ManaStream");
				if (relayReclaimSound != null) Instantiate(relayReclaimSound, hit.transform.position,new Quaternion());
				Destroy(hit.transform.gameObject);

				ResourceManager.SINGLETON.ReturnRelay();
			}
			else if (ResourceManager.SINGLETON.UseAvailableRelay())
			{
				if (Physics.Raycast(ray, out hit, 100, relayLayer))
				{
					PrintDebugMsg("Hit the relay layer.");
					Vector3 spawnPoint = new Vector3(hit.point.x, hit.point.y, hit.point.z + offset);
					Instantiate(relay, spawnPoint, Quaternion.identity);
					if(relaySound!=null) Instantiate(relaySound, spawnPoint, Quaternion.identity);
				}
			}
		}
    }

	public void TogglePause()
	{
		if (pauseScreen.gameObject.activeInHierarchy == false)
		{
			pauseScreen.gameObject.SetActive(true);
			Time.timeScale = 0;
			paused = true;
		}
		else
		{
			pauseScreen.gameObject.SetActive(false);
			Time.timeScale = 1;
			paused = false;
		}
	}

    /// <summary>
    /// Checks for user's input and calls properfunction.
    /// </summary>
    private void CheckInput()
    {
        if (Input.GetButtonDown("StartMana")) StartMana();
        if (Input.GetButtonDown("Fire1")) SpawnRelay();
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			TogglePause();
		}
    }

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Use Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        if (isDebugGameController) Debug.Log(((this.GetType().Name != debugScriptNameGameController) ? this.GetType().Name + "->" : "") + debugScriptNameGameController + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintWarningDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogWarning(((this.GetType().Name != debugScriptNameGameController) ? this.GetType().Name + "->" : "") + debugScriptNameGameController + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintErrorDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogError(((this.GetType().Name != debugScriptNameGameController) ? this.GetType().Name + "->" : "") + debugScriptNameGameController + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties

    #endregion
    #endregion

    #region Start & Update Functions
    void Awake()
    {
        PrintDebugMsg("Debugging enabled.");

        if (GameController.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg("More than one GameController singleton found!");
    }
    private void Start()
    {
        GameObject start = GameObject.Find("Start");
        for(int i = 0; i < start.transform.childCount; i++)
        {
            Animator animator = start.transform.GetChild(i).GetComponent<Animator>();
            if (animator != null) riftAnimator = animator;
        }
    }
    private void Update()
    {
        CheckInput();
    }
    #endregion
}