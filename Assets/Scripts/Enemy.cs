﻿/* Created by: Matthew George
 */

using UnityEngine;
using System.Collections.Generic;

public enum EnemyState
{
    Idle, // Patroling
    Aggro // Chasing mana
}

/// <summary>
/// 
/// </summary>
public class Enemy : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebugEnemy = false;
    private string debugScriptNameEnemy = "Enemy";
    #endregion

    #region Public
    [SerializeField] private Transform endPointOne = null;
    [SerializeField] private Transform endPointTwo = null;
    [SerializeField] private float destDist = 1;
    [SerializeField] private float speed = 1;
    [SerializeField] private float manaDetRange = 3;
    [SerializeField] private Transform[] goals = null;
	public GameObject agroSound;
    #endregion

    #region Private
    private EnemyState state = EnemyState.Idle;
    private Vector3 currTargetPoint = Vector3.zero;
    private Transform manaHead = null;
    private bool looseCond = false;
    private Vector3 goalPos = Vector3.zero;
    private float currSpeed = 0;
    private Animator animator = null;
    private SpriteRenderer spriteRenderer = null;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Called when this enemy reaches the tree. Stops and plays an animation (not implemented).
    /// </summary>
    public void ReachedGoal()
    {
        currSpeed = 0;
    }
    /// <summary>
    /// Resets speed to starting speed.
    /// </summary>
    public void ResetSpeed()
    {
        currSpeed = speed;
    }

    #region States
    /// <summary>
    /// Begin idling by choosing the nearest end point to target.
    /// </summary>
    private void StartIdle()
    {
        float endPOneDist = Vector3.Distance(transform.position, endPointOne.position);
        float endPTwoDist = Vector3.Distance(transform.position, endPointTwo.position);
        if(endPOneDist <= endPTwoDist)
        {
            PrintDebugMsg("Choosing end point one...");
            currTargetPoint = endPointOne.position;
        }
        else
        {
            PrintDebugMsg("Choosing end point two...");
            currTargetPoint = endPointTwo.position;
        }
        
        animator.SetBool("isAggro", false);
    }
    /// <summary>
    /// Begin aggroing by setting the found mana head as the target.
    /// </summary>
    private void StartAggro()
    {
        if (Vector3.Distance(transform.position, manaHead.position) <= manaDetRange)
        {
            PrintDebugMsg("Mana head sensed!");
            currTargetPoint = manaHead.transform.position;
            State = EnemyState.Aggro;
            
            animator.SetBool("isAggro", true);
			Instantiate(agroSound);
        }
        else
        {
            State = EnemyState.Idle;
            animator.SetBool("isAggro", false);
        }
    }

    /// <summary>
    /// Look for any mana within range.
    /// </summary>
    private void LookForMana()
    {
        if(manaHead == null) manaHead = GameObject.FindGameObjectWithTag("ManaStream").transform;
        if (manaHead != null)
        {
            PrintDebugMsg("Mana head found.");
            StartAggro();

            List<GameObject> relays = manaHead.GetComponent<ManaTrail>().movePath;
            if (relays.Count > 0)
            {
                foreach (Transform goal in goals)
                {
                    if (relays[relays.Count - 1] == goal.gameObject)
                    {
                        PrintDebugMsg("Loose condition active.");
                        goalPos = goal.position;
                        looseCond = true;
                        break;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Move towards target point.
    /// </summary>
    private void Move()
    {
        Vector3 dir = -(transform.position - currTargetPoint).normalized;
        if (dir.x > 0) spriteRenderer.flipX = true;
        else if (dir.x <= 0) spriteRenderer.flipX = false;
        if (isDebugEnemy) Debug.DrawRay(transform.position, dir, Color.blue);

        transform.Translate(dir * currSpeed * Time.deltaTime);
    }
    /// <summary>
    /// Check if reached destination and determine what to do next if true.
    /// </summary>
    private void CheckDestination()
    {
        if(isDebugEnemy) Debug.DrawLine(endPointOne.position, endPointTwo.position, Color.green);

        if(Vector3.Distance(transform.position, currTargetPoint) <= destDist)
        {
            PrintDebugMsg("Reached destination.");

            switch(state)
            {
                case EnemyState.Idle:
                    if (currTargetPoint == endPointOne.position) currTargetPoint = endPointTwo.position;
                    else if (currTargetPoint == endPointTwo.position) currTargetPoint = endPointOne.position;
                    break;
                case EnemyState.Aggro:
                    if(GameController.SINGLETON.manaActive) currTargetPoint = manaHead.position;
                    break;
            }
        }

        Move();
    }

    /// <summary>
    /// Check what state it is and perform it.
    /// </summary>
    private void HandleState()
    {
        switch(state)
        {
            case EnemyState.Idle:
                if (GameController.SINGLETON.manaActive) LookForMana();
                break;
            case EnemyState.Aggro:
                if (GameController.SINGLETON.manaActive && Vector3.Distance(transform.position, currTargetPoint) > destDist) currTargetPoint = manaHead.position;
                else if (!GameController.SINGLETON.manaActive)
                {
                    if (looseCond)
                    {
                        PrintDebugMsg("Going to goal...");
                        currTargetPoint = goalPos;
                    }
                    else State = EnemyState.Idle;
                }
                break;
        }

        CheckDestination();
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Use Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        if (isDebugEnemy) Debug.Log(((this.GetType().Name != debugScriptNameEnemy) ? this.GetType().Name + "->" : "") + debugScriptNameEnemy + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintWarningDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogWarning(((this.GetType().Name != debugScriptNameEnemy) ? this.GetType().Name + "->" : "") + debugScriptNameEnemy + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintErrorDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogError(((this.GetType().Name != debugScriptNameEnemy) ? this.GetType().Name + "->" : "") + debugScriptNameEnemy + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    public EnemyState State
    {
        set
        {
            if(value != state)
            {
                switch(value)
                {
                    case EnemyState.Idle:
                        PrintDebugMsg("Now idling...");
                        StartIdle();
                        break;
                    case EnemyState.Aggro:
                        PrintDebugMsg("Now aggroing...");
                        break;
                }

                state = value;
            }
        }
        get
        {
            return state;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    void Awake()
    {
        PrintDebugMsg("Debugging enabled.");
    }
    private void Start()
    {
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        currSpeed = speed;
        StartIdle();
    }
    private void Update()
    {
        HandleState();
    }
    #endregion
}