﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seed : MonoBehaviour {
	public ManaTree manaTree;
	public string EnemyTag = "Enemy";
	public string manaStreamTag = "ManaStream";

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == EnemyTag)
		{
			manaTree.LoseGame();
			other.GetComponent<Enemy>().ReachedGoal();
		}
		else if (other.tag == manaStreamTag)
		{
			other.GetComponent<ManaTrail>().Die();
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            bool enemyAggroed = false;
            foreach(GameObject enemy in enemies)
            {
                Enemy enemyScript = enemy.GetComponent<Enemy>();
                if(enemyScript.State == EnemyState.Aggro)
                {
                    enemyAggroed = true;
                    break;
                }
            }
            if(!enemyAggroed) manaTree.WinGame();
		}
	}
}
