﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaTree : MonoBehaviour {
    public Animator animTree;
    public string gameOverAnimBoolName = "GameOver";
    public GameObject treeDieSound;
    public float reloadLevelDelay = 1;
    public float gameOverDelay = 1;

    public void WinGame()
    {
        Invoke("ReloadLevel", reloadLevelDelay);
    }

    public void LoseGame()
    {
        Instantiate(treeDieSound, transform.position, Quaternion.identity);
        animTree.SetBool(gameOverAnimBoolName, true);
        Invoke("ShowGameOverScreen", gameOverDelay);
    }

    public void ReloadLevel()
    {
        CrossSceneInfo.SINGLETON.LoadNextScene();
    }
    public void ShowGameOverScreen()
    {
        CrossSceneInfo.SINGLETON.ShowLooseScreen();
    }
}
