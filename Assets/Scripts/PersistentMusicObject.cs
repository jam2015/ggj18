﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentMusicObject : MonoBehaviour {
	public static PersistentMusicObject inst;
	// Use this for initialization
	void Awake() {
		if (PersistentMusicObject.inst == null)
		{
			PersistentMusicObject.inst = this;
		}
		else
		{
			Destroy(this.gameObject);
		}
		DontDestroyOnLoad(PersistentMusicObject.inst.gameObject);
	}
}
