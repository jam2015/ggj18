﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagScanner : MonoBehaviour {
	public string scanTag;
	public bool activated=false;
	public GameObject refGameobject;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!activated && refGameobject != null)
		{
			var obj = GameObject.FindGameObjectWithTag(scanTag);
			if (obj != null)
			{
				refGameobject.GetComponent<TextPanel>().Progress();
			}
		}
	}
}
