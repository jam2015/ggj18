﻿/* Created by: Matthew George
 */

using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// 
/// </summary>
public class StartScreen : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebugStartScreen = false;
    private string debugScriptNameStartScreen = "StartScreen";
    #endregion

    #region Public
    [SerializeField] private GameObject startScreen = null;
    [SerializeField] private GameObject credits = null;
    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    public void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
        if(CrossSceneInfo.SINGLETON != null) Destroy(CrossSceneInfo.SINGLETON.gameObject);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void ToggleCredits()
    {
        if (credits.activeInHierarchy)
        {
            startScreen.SetActive(true);
            credits.SetActive(false);
        }
        else
        {
            startScreen.SetActive(false);
            credits.SetActive(true);
        }
    }

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Use Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        if (isDebugStartScreen) Debug.Log(((this.GetType().Name != debugScriptNameStartScreen) ? this.GetType().Name + "->" : "") + debugScriptNameStartScreen + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintWarningDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogWarning(((this.GetType().Name != debugScriptNameStartScreen) ? this.GetType().Name + "->" : "") + debugScriptNameStartScreen + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintErrorDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogError(((this.GetType().Name != debugScriptNameStartScreen) ? this.GetType().Name + "->" : "") + debugScriptNameStartScreen + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties

    #endregion
    #endregion

    #region Start & Update Functions
    void Awake()
    {
        PrintDebugMsg("Debugging enabled.");
    }
    private void Start()
    {
        startScreen.SetActive(true);
        credits.SetActive(false);
    }
    #endregion
}