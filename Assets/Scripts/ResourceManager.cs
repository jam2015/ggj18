﻿/* Created by: Matthew George
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class ResourceManager : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebugResourceManager = false;
    private string debugScriptNameResourceManager = "ResourceManager";
    #endregion

    #region Public
    public static ResourceManager SINGLETON = null;

    [SerializeField] private int startRelays = 7;
    [SerializeField] private Transform content = null;
    [SerializeField] private GameObject relayImage = null;
    [Range(0, 1)]
    [SerializeField] private float alphaPerc = .5f;
    #endregion

    #region Private
    private int currRelaysUsed = 0;
    private List<Image> relays = null;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Checks to see if any relays are available to spawn and then uses one if available.
    /// </summary>
    /// <returns>True if relays available.</returns>
    public bool UseAvailableRelay()
    {
        if (currRelaysUsed >= startRelays)
        {
            PrintDebugMsg("No more relays available.");
            return false;
        }
        else
        {
            PrintDebugMsg((startRelays - currRelaysUsed) + " relays available. Using one...");
            currRelaysUsed++;
            UpdateUI();
            return true;
        }
    }
    /// <summary>
    /// Makes a relay available again. Does not remove the object.
    /// </summary>
    public void ReturnRelay()
    {
        PrintDebugMsg("Returning relay... " + (startRelays - currRelaysUsed) + " now available.");
        currRelaysUsed--;
        UpdateUI();
    }

    /// <summary>
    /// Sets up the list of available relays.
    /// </summary>
    private void SetupUI()
    {
        relays = new List<Image>();
        for(int i = 0; i < startRelays; i++)
        {
            Image imgRelay = Instantiate(relayImage, content).GetComponent<Image>();
            relays.Add(imgRelay);
        }
    }
    /// <summary>
    /// updates the UI.
    /// </summary>
    private void UpdateUI()
    {
        for(int i = 0; i < relays.Count; i++)
        {
            if (i >= currRelaysUsed) relays[i].color = new Color(relays[i].color.r, relays[i].color.g, relays[i].color.b, 1);
            else relays[i].color = new Color(relays[i].color.r, relays[i].color.g, relays[i].color.b, alphaPerc);
        }
    }

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Use Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        if (isDebugResourceManager) Debug.Log(((this.GetType().Name != debugScriptNameResourceManager) ? this.GetType().Name + "->" : "") + debugScriptNameResourceManager + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintWarningDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogWarning(((this.GetType().Name != debugScriptNameResourceManager) ? this.GetType().Name + "->" : "") + debugScriptNameResourceManager + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintErrorDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogError(((this.GetType().Name != debugScriptNameResourceManager) ? this.GetType().Name + "->" : "") + debugScriptNameResourceManager + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    
    #endregion
    #endregion

    #region Start & Update Functions
    void Awake()
    {
        PrintDebugMsg("Debugging enabled.");

        if (ResourceManager.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg("More than one ResourceManager singleton found!");
    }
    private void Start()
    {
        SetupUI();
        UpdateUI();
    }
    #endregion
}