﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TextPanel : MonoBehaviour {
	bool stillDisplaying = true;
	public Text text;
	public Tutorial tutorialBrain;
	[TextArea]
	public string textToDisplay;
	public bool waitForAction = false;

	private void Awake()
	{
		text.text = textToDisplay;
		StartCoroutine(FadeTextToFullAlpha(3f, text));
	}

	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.Return))
		{
			if (stillDisplaying)
			{
				stillDisplaying = false;
			}
			else
			{
				if (!waitForAction)
				{
					tutorialBrain.NextStep();
				}
			}
		}
	}

	public void Progress()
	{
		tutorialBrain.NextStep();
	}

	public IEnumerator FadeTextToFullAlpha(float t, Text i)
	{
		i.color = new Color(i.color.r, i.color.g, i.color.b, 0);
		while (i.color.a < 1.0f)
		{
			i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a + (Time.deltaTime / t));
			

			if (stillDisplaying == false)
			{
				i.color = new Color(i.color.r, i.color.g, i.color.b, 1.0f);
			}

			yield return null;
		}
		stillDisplaying = false;
	}



	public IEnumerator FadeTextToZeroAlpha(float t, Text i)
	{
		i.color = new Color(i.color.r, i.color.g, i.color.b, 1);
		while (i.color.a > 0.0f)
		{
			i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a - (Time.deltaTime / t));


			if (stillDisplaying == false)
			{
				i.color = new Color(i.color.r, i.color.g, i.color.b, 1.0f);
			}

			yield return null;
		}
	}
}
